<?php
/**
 * MoIP - Moip Payment Module
 *
 * @title      Magento -> Custom Payment Module for Moip (Brazil)
 * @category   Payment Gateway
 * @package    O2TI_Moip
 * @author     O2ti solucoes web ldta
 * @copyright  Copyright (c) 2010 MoIP Pagamentos S/A
 * @license    Autorizado o uso por tempo indeterminado
 */
class O2TI_Moip_StandardController extends Mage_Core_Controller_Front_Action {
	public function getOrder() {
		if ($this->_order == null) {}
		return $this->_order;
	}
	public function getStandard() {
		return Mage::getSingleton('moip/standard');
	}
	protected function _expireAjax() {
		if (!Mage::getSingleton('checkout/session')->getQuote()->hasItems()) {
			$this->getResponse()->setHeader('HTTP/1.1', '403 Session Expired');
			exit;
		}
	}
	public function redirectAction() {
		$session = Mage::getSingleton('checkout/session');
		$standard = $this->getStandard();
		$fields = $session->getMoIPFields();
		$fields['id_transacao'] = Mage::getSingleton('checkout/session')->getLastRealOrderId();
		$pgtoArray = $session->getPgtoArray();
		$api = Mage::getModel('moip/api');
		$api->setAmbiente($standard->getConfigData('ambiente'));
		$xml = $api->generateXML($fields, $pgtoArray);
		Mage::register('xml', $xml);
		$formapgto = $api->generateforma($fields, $pgtoArray);
		Mage::register('formapgto', $formapgto);
		$formapg = $api->generateformapg($fields, $pgtoArray);
		Mage::register('formapg', $formapg);
		$token = $api->getToken($xml);
		$session->setMoipStandardQuoteId($session->getQuoteId());
		Mage::register('token', $token['token']);
		Mage::register('erro', $token['erro']);
		Mage::register('StatusPgdireto', $token['pgdireto_status']);
		$this->loadLayout();
		$this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
		$this->getLayout()->getBlock('content')->append($this->getLayout()->createBlock('moip/standard_redirect'));
		$this->renderLayout();
		$session->unsQuoteId();
	}
	/**
	 * Quando um cliente cancelar o pagamento da Moip
	 */
	public function cancelAction() {
		$session = Mage::getSingleton('checkout/session');
		$session->setQuoteId($session->getMoipStandardQuoteId(true));

		if ($session->getLastRealOrderId()) {
			$order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
			if ($order->getId()) {
				$order->cancel()->save();
			}
		}
		$this->_redirect('checkout/cart');
	}
	/**
	 * Quando há retorno para o Módulo. A informação da ordem neste momento é em Variáveis via POST. No entanto, você não quer "processar" o pedido até o obter a validação do IPN
	 */
	public function successAction() {
		$standard = $this->getStandard();
		$order = Mage::getModel('sales/order');
		$session = Mage::getSingleton('checkout/session');
		if (!$this->getRequest()->isPost()) {
			$session->setQuoteId($session->getMoipStandardQuoteId(true));
			/**
			 * definir a citação como inativos após a volta do Módulo
			 */
			Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
			/**
			 * Enviar e-mail de confirmação para o cliente
			 */
			$order->load(Mage::getSingleton('checkout/session')->getLastOrderId());
			if ($order->getId()) {
			}
			/**
			 * Faz o redirecionamento para a tela de compra efetuada
			 */
			$this->_redirect('checkout/onepage/success', array('_secure' => true));
		} else {
			$data = $this->getRequest()->getPost();
			/**
			 * Efetua a mudança do Status
			 */
			$login = $standard->getConfigData('conta_moip');
			$order->loadByIncrementId(ereg_replace($login, "", $data['id_transacao']));
			/*
              const STATE_NEW        = 'new';
              const STATE_PROCESSING = 'processing';
              const STATE_COMPLETE   = 'complete';
              const STATE_CLOSED     = 'closed';
              const STATE_CANCELED   = 'canceled';
              const STATE_HOLDED     = 'holded';
             */
			$LastRealOrderId = ereg_replace($login, "", $data['id_transacao']);
			$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT * FROM moip WHERE sale_id IN (".$LastRealOrderId.") AND status ='Sucesso'";
			$_venda = $conn->fetchAll($sql);
			foreach ($_venda as $venda) {
				$tokenpagamento = $venda['xml_return'];
				$Formadepagamento = $venda['formapg'];
				$bandeira = $venda['bandeira'];
			}
			switch ($data['status_pagamento']) {
			case 1:
				if ($_SERVER['SERVER_ADDR'] = "208.82.206.66") {
					$state = Mage_Sales_Model_Order::STATE_PROCESSING;
					$status = 'processing';
					$comment = $this->getStatusPagamentoMoip($data['status_pagamento']);
					$comment = $comment ." Pagamento realizado por: ". $this->getNomePagamento($Formadepagamento);
					$comment = $comment ."\n Via instuição: ". $bandeira;
					$comment =  $comment; "\n ID MOIP" .$data['cod_moip']. "\n Pagamento direto no MoiP https://www.moip.com.br/Instrucao.do?token=" .$LastRealOrderId;
					//Inicia geração da fatura
					$invoice = $order->prepareInvoice();
					if ($this->getStandard()->canCapture()) {
						$invoice->register()->capture();
					}
					Mage::getModel('core/resource_transaction')
					->addObject($invoice)
					->addObject($invoice->getOrder())
					->save();
					$invoice->sendEmail();
					$invoice->setEmailSent(true);
					$invoice->save();
					//encerra geração da fatura! salve o tricolor paulista!
					Mage::dispatchEvent('moip_order_authorize', array("order" => $order));
				}
				else {
					$state = Mage_Sales_Model_Order::STATE_CANCELED;
					$status = 'canceled';
					$comment = "Tentativa de Fraude no retorno Moip";
					$comment = $comment . "\n Pagamento direto no MoiP https://www.moip.com.br/Instrucao.do?token=" .$tokenpagamento;
					$order->cancel();
				}
				break;
			case 2:
				$state = Mage_Sales_Model_Order::STATE_HOLDED;
				$status = 'holded';
				$comment = $this->getStatusPagamentoMoip($data['status_pagamento']);
				$comment = $comment ." Pagamento realizado por: ". $this->getNomePagamento($Formadepagamento);
				$comment = $comment ."\n Via instuição: ". $bandeira;
				$comment = $comment. "\n ID MOIP " .$data['cod_moip']. "\n Pagamento direto no MoiP https://www.moip.com.br/Instrucao.do?token=" .$tokenpagamento;
				Mage::dispatchEvent('moip_order_canceled_fraud', array("order" => $order));
				break;
			case 3:
				$state = Mage_Sales_Model_Order::STATE_HOLDED;
				$status = 'holded';
				$comment = $this->getStatusPagamentoMoip($data['status_pagamento']);
				$comment = $comment. "\n ID MOIP " .$data['cod_moip']. "\n Reimprimir boleto https://www.moip.com.br/Boleto.do?id=" .$data['cod_moip'] . "\n Pagamento direto no MoiP https://www.moip.com.br/Instrucao.do?token=" .$tokenpagamento;
				Mage::dispatchEvent('moip_order_hold_printed', array("order" => $order));
				break;
			case 4:
				$state = Mage_Sales_Model_Order::STATE_PROCESSING;
				$status = 'processing';
				$comment = $this->getStatusPagamentoMoip($data['status_pagamento']);
				$comment = $comment ." Pagamento realizado por: ". $this->getNomePagamento($Formadepagamento);
				$comment = $comment ."\n Via instuição: ". $bandeira;
				$comment = $comment. "\n ID MOIP " .$data['cod_moip']. "\n Pagamento direto no MoiP https://www.moip.com.br/Instrucao.do?token=" .$tokenpagamento;
				Mage::dispatchEvent('moip_order_closed', array("order" => $order));
				break;
			case 5:
				$state = Mage_Sales_Model_Order::STATE_CANCELED;
				$status = 'canceled';
				$comment = $this->getStatusPagamentoMoip($data['status_pagamento']);
				$comment = $comment ." Pagamento realizado por: ". $this->getNomePagamento($Formadepagamento);
				$comment = $comment ."\n Via instuição: ". $bandeira;
				$comment = $comment . "\n ID MOIP " .$data['cod_moip']. "\n Motivo: ".utf8_encode($data['classificacao'])."\n Pagamento direto no MoiP https://www.moip.com.br/Instrucao.do?token=" .$tokenpagamento;
				$order->cancel();
				Mage::dispatchEvent('moip_order_canceled', array("order" => $order));
				break;
			case 6:
				$state = Mage_Sales_Model_Order::STATE_HOLDED;
				$status = 'holded';
				$comment = $this->getStatusPagamentoMoip($data['status_pagamento']);
				$comment = $comment ." Pagamento realizado por: ". $this->getNomePagamento($Formadepagamento);
				$comment = $comment ."\n Via instuição: ". $bandeira;
				$comment = $comment. "\n ID MOIP " .$data['cod_moip']. " Pagamento direto no MoiP https://www.moip.com.br/Instrucao.do?token=" .$tokenpagamento;
				Mage::dispatchEvent('moip_order_holded_review', array("order" => $order));
				break;
			}
			$order->setState($state, $status, $comment, $notified = true, $includeComment = true);
			$order->save();
			$order->load(Mage::getSingleton('checkout/session')->getLastOrderId());
			if ($order->getId()) {}
			Zend_Debug::dump('Processo de retorno concluido!');
		}
	}
	private function getNomePagamento($param) {
		$nome = "";
		switch ($param) {
		case "BoletoBancario":
			$nome = "Boleto Bancário";
			break;
		case "DebitoBancario":
			$nome = "Debito Bancário";
			break;
		case "CartaoCredito":
			$nome = "Cartão de Crédito";
			break;
		}
		return $nome;
	}
	private function getStatusPagamentoMoip($param) {
		$status = "";
		switch ($param) {
		case 1:
			$status = "Autorizado";
			break;
		case 2:
			$status = "Iniciado";
			break;
		case 3:
			$status = "Boleto Impresso";
			break;
		case 4:
			$status = "Concluido";
			break;
		case 5:
			$status = "Cancelado";
			break;
		case 6:
			$status = "Em análise";
			break;
		case 7:
			$status = "Estornado";
			break;
		}
		return $status;
	}

	public function buscaCepAction() {
		if ($_POST['meio'] != "") {

			include 'phpQuery-onefile.php';
			function simple_curl($url, $post=array(), $get=array()) {
				$url = explode('?', $url, 2);
				if (count($url)===2) {
					$temp_get = array();
					parse_str($url[1], $temp_get);
					$get = array_merge($get, $temp_get);
				}
				$ch = curl_init($url[0]."?".http_build_query($get));
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				return curl_exec($ch);
			}
			$cep = $_POST['s'];
			$vSomeSpecialChars = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ç", "Ç", "ã", "Ã", "õ", "Õ");
			$vReplacementChars = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "c", "C", "a", "A", "o", "O");
			$cep = str_replace($vSomeSpecialChars, $vReplacementChars, $cep);
			$cep = preg_replace('/[^\p{L}\p{N}]/u', '+', $cep);
			$html = simple_curl('http://m.correios.com.br/movel/buscaCepConfirma.do', array(
					'cepEntrada'=>''.utf8_encode($cep).'',
					'metodo'=>'buscarCep'
				));
			Mage::getModel('Moip/PhpQuery')->newDocumentHTML($html, $charset = 'utf-8');
			echo $html;
		} else {
			function simple_curl($url, $post=array(), $get=array()) {
				$url = explode('?', $url, 2);
				if (count($url)===2) {
					$temp_get = array();
					parse_str($url[1], $temp_get);
					$get = array_merge($get, $temp_get);
				}
				$ch = curl_init($url[0]."?".http_build_query($get));
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				return curl_exec($ch);
			}

			$cep = $_GET['cep'];
			$html = simple_curl('http://m.correios.com.br/movel/buscaCepConfirma.do', array(
					'cepEntrada'=>$cep,
					'tipoCep'=>'',
					'cepTemp'=>'',
					'metodo'=>'buscarCep'
				));
			$dom = new Zend_Dom_Query($html);
			$results = $dom->query('.caixacampobranco .respostadestaque');
			
			
			$counter = 0;
			foreach($results as $result){
				switch($counter){
					case 0:
						$logradouro = trim(utf8_decode($result->textContent));
						$logradouro = explode("-", $logradouro);
						$logradouro = trim($logradouro[0]);
					break;
					case 1:
						$bairro = trim(utf8_decode($result->textContent));
					break;
					case 2:
						$cidadeUf = trim(utf8_decode($result->textContent));
					break;
					case 3:
						$cep = trim(utf8_decode($result->textContent));
					break;
				}
				$counter++;
			}
			
			$cidadeUF = explode('/', $cidadeUf);
			
			$dados =
				array(
				"logradouro"=> $logradouro,
				"bairro"=> $bairro,
				"cidade" => trim($cidadeUF[0]),
				"uf"=> trim($cidadeUF[1]),
				"cep"=> $cep
			);
			
			switch ($dados['uf']){
				case "AC":
				$estado = "485";
				break;
				case "AL":
				$estado = "486";
				break;
				case "AP":
				$estado = "487";
				break;
				case "AM":
				$estado = "488";
				break;
				case "BA":
				$estado = "489";
				break;
				case "CE":
				$estado = "490";
				break;
				case "DF":
				$estado = "491";
				break;
				case "ES":
				$estado = "492";
				break;
				case "GO":
				$estado = "493";
				break;
				case "MA":
				$estado = "494";
				break;
				case "MT":
				$estado = "495";
				break;
				case "MS":
				$estado = "496";
				break;
				case "MG":
				$estado = "497";
				break;
				case "PA":
				$estado = "498";
				break;
				case "PB":
				$estado = "499";
				break;
				case "PR":
				$estado = "500";
				break;
				case "PE":
				$estado = "501";
				break;
				case "PI":
				$estado = "502";
				break;
				case "RJ":
				$estado = "503";
				break;
				case "RN":
				$estado = "504";
				break;
				case "RS":
				$estado = "505";
				break;
				case "RO":
				$estado = "506";
				break;
				case "RR":
				$estado = "507";
				break;
				case "SC":
				$estado = "508";
				break;
				case "SP":
				$estado = "509";
				break;
				case "SE":
				$estado = "510";
				break;
				case "TO":;
				$estado = "511";
				break;
			}   
			if ($dados['logradouro'] != "") {
				$separa_end = explode('- ', $dados['logradouro']);
				if ($dados['uf'] != ""):
				$texto = $dados['tipo_logradouro']." ".utf8_decode($separa_end[0]).":".$dados['bairro'].":".$dados['cidade'].":".$estado.";";
				else:
				$texto = $dados['tipo_d']." :".$dados['d'].":".$dados['logradouro'].":".$estado.";";
				endif;
			}
			
			echo $texto;
			//$this->getResponse()->setBody(Zend_Json::encode($dados));
		}

	}
}
