<?php
class O2TI_Moip_Block_Standard_Redirect extends Mage_Core_Block_Abstract {
	protected function _toHtml() {
		$data = $this->getRequest()->getParams();
		$standard = Mage::getModel('moip/standard');
		$form = new Varien_Data_Form();
		$api = Mage::getModel('moip/api');
		$api->setAmbiente($standard->getConfigData('ambiente'));
		$idgoogle = $standard->getConfigData('idgoogle');
		$meio = $api->generatemeip(Mage::registry('formapgto'));
		$opcaopg = $api->generatemeiopago(Mage::registry('formapg'));
		$parcelamento = $standard->getInfoParcelamento();
		$enviapara = $standard->getConfigData('ambiente');
		
		if($enviapara == "teste"){
		$urldomoip = "https://desenvolvedor.moip.com.br/sandbox";
		echo Mage::registry('xml');
		echo Mage::registry('erro');
		echo Mage::registry('token');	
		}
		else
		{
		$urldomoip = "https://www.moip.com.br/";
		}
		if($opcaopg['forma_pagamento'] == "DebitoBancario"){
			$bandeira = $opcaopg['debito_instituicao'];
			$vencpedido = date('c', strtotime("+2 days"));
		}
		if($opcaopg['forma_pagamento'] == "CartaoCredito"){
			$bandeira = $opcaopg['credito_instituicao'];
			$vencpedido = date('c', strtotime("+2 days"));
		}
		if($opcaopg['forma_pagamento'] == "BoletoBancario"){
			$bandeira = "Bradesco";
			$vencpedido = $opcaopg['timevencimentoboleto'];
		}
		$status_pgdireto = Mage::registry('StatusPgdireto');
		$html = $this->__('');
		if (!$data['token']){

			//primeira compra grava os dados.

			if (!$moipToken){
				$moipToken = Mage::registry('token');
				$url = $moipToken;
				$xmlgerado = Mage::registry('xml');
				$xml_sent = (string)$xmlgerado;
				$standard = Mage::getSingleton('moip/standard');
				$sale_id =  Mage::getSingleton('checkout/session')->getLastRealOrderId();
				$realorder_id =  Mage::getSingleton('checkout/session')->getLastOrderId();
				$incrementId = $realorder_id;
				$status = "Sucesso";
				$connR = Mage::getSingleton('core/resource')->getConnection('core_read');
				$sql = "SELECT *
	          FROM moip
	          WHERE sale_id IN (".$sale_id.") AND status ='Sucesso'";
				$_venda = $connR->fetchAll($sql);
				foreach($_venda as $venda)
				{
					$tokenpagamento = $venda['xml_return'];
				}
				if($tokenpagamento == "")
				{
					$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
					$results = $conn->query(
						"INSERT INTO `moip` (
			`transaction_id` ,
			`realorder_id` ,
			`sale_id` ,
			`xml_sent` ,
			`xml_return` ,
			`status` ,
			`formapg` ,
			`bandeira` ,
			`digito` ,
			`vencimento` ,
			`datetime`
			)
			VALUES (
			NULL , '".$realorder_id."', '".$sale_id."', '".$xml_sent."', '".$moipToken."', '".$status."', '".$opcaopg['forma_pagamento']."', '".$bandeira."', '', '".$vencpedido."', '".date('Y-m-d H:i:s')."'
			);"
					);
					$tokenpagamento = $moipToken;
					$url = $moipToken;

				}
			}
			else {
				//usado para o reload da pagina pelos infelizes clientes que navegam com ie.
				$tokenpagamento = $moipToken;
				$url = $tokenpagamento;
				$status = "Sucesso";
			}
		}

		//refaz o pedido vindo do my account.
		else {

			$LastRealOrderId = $data['realorder'];
			$conn = Mage::getSingleton('core/resource')->getConnection('core_read');
			$sql = "SELECT *
			  FROM moip
			  WHERE sale_id IN (".$LastRealOrderId.")";
			$_venda = $conn->fetchAll($sql);
			foreach($_venda as $venda)
			{
				$tokenpagamento = $venda['xml_return'];
				$url = $venda['xml_return'];
				$incrementId = $venda['realorder_id'];
				$order = $data['realorder'];
				$opcaopg['forma_pagamento'] = $venda['formapg'];
				$bandeira = $venda['bandeira'];
				$connRW = Mage::getSingleton('core/resource')->getConnection('core_write');
				$results = $connRW->query("UPDATE `moip` SET vencimento='".$vencpedido."' WHERE sale_id IN (".$LastRealOrderId.");");
			}
			$refazerpagamento = 1;
			$url = $venda['xml_return'];
		}

		$order = Mage::getModel('sales/order')->load($incrementId);
		$session = Mage::getSingleton('customer/session');
		$customer = $session->getCustomer();
		try{
			$order->sendNewOrderEmail();
		}
		catch (Exception $ex) {  };
		$oque = $order->getIsVirtual();
		if ($oque){
			$shippingId = $order->getBillingAddress()->getId();
		}
		else {
			$shippingId = $order->getShippingAddress()->getId();
		}
		$address = Mage::getModel('sales/order_address')->load($shippingId);
		$urldosite = Mage::getBaseUrl('web', true);
		$totalparaparcelado = $order->getGrandTotal();
?>
	<!-- entrou no pagamento -->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php echo $idgoogle ?>']);
		_gaq.push(['_setDomainName', '<?php echo $urldosite ?>']);
		_gaq.push(['_addIgnoreRef', 'seusite.com.br']);//dominio para evitar duplicidade
		_gaq.push(['_trackPageview']);
		_gaq.push(['_addTrans',
			'<?php echo $sale_id ?>',
			'<?php echo Mage::app()->getStore() ?>',
			'<?php echo ($totalparaparcelado - $order->getShippingAmount()) ?>',
			'0',
			'<?php echo $order->getShippingAmount() ?>',
			'<?php echo $address->getCity() ?>',
			'<?php echo $address->getRegion() ?>',
			'<?php echo $address->getCountry_id() ?>'
		]);
		<?php $orderItems = $order -> getItemsCollection();
		foreach ($orderItems as $item) {
			$product_sku = $item -> sku;
			$product_name = $item -> getName();
			$_product = Mage::getModel('catalog/product') -> load($item->getProductId());
			$cats = $_product -> getCategoryIds();
			$category_name = Mage::getModel('catalog/category') -> load($cats[0]) -> getName();
			$price = $item->getPrice();
			$qty = $item -> getQtyOrdered();
			echo "
			_gaq.push(['_addItem',
			'$incrementId',
			'$product_sku',
			'$product_name',
			'$category_name',
			'$price',
			'$qty'
			]);
			";
		}
?>
		_gaq.push(['_trackTrans']);
		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	
	
	
	
		<!-- Corpo comum ao pagamento -->
	<div id="moip_corpo">
	<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/jquery.js'); ?>"></script>
	<script type='text/javascript' charset="ISO-8859-1" src='<?php echo $urldomoip ?>/transparente/MoipWidget-v2.js'></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->getSkinUrl('o2ti_moip/css/bootstrap.css'); ?> "media="all">	
	<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/bootstrap.min.js'); ?>"></script>
	<div id="MoipWidget" data-token="<?php echo $url ?>" callback-method-error="erroValidacao" callback-method-success="sucesso"  ></div>
	<h2>Pedido realizado via Moip S/A</h2>
	<div id="icone_pg" >
	<img src="<?php echo $this->getSkinUrl('o2ti_moip/imagem/logomoip.png'); ?>" border="0" >
	<img src="<?php echo $this->getSkinUrl('o2ti_moip/imagem/'.$bandeira.'.png'); ?>" border="0" >
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#resultado").hide();
		$("#token").val("<?php echo $url; ?>");
		$("#boleto").ready(function(){
			sendTomoip();
		});
	});
	sendTomoip = function() {
		var settings = {
			<?php if($refazerpagamento != "1") {   echo $meio; }  ?>
<?php if($refazerpagamento == "1" && $opcaopg['forma_pagamento'] == "BoletoBancario" ) { echo ('"Forma": "BoletoBancario"');} ?>
<?php if($refazerpagamento == "1" && $opcaopg['forma_pagamento'] == "DebitoBancario" ) { echo ('"Forma": "DebitoBancario","Instituicao":"'.$bandeira.'"');} ?>

		};
		MoipWidget(settings);
<?php if($refazerpagamento == "1" && $opcaopg['forma_pagamento'] == "CartaoCredito" ) { echo ('$("#refazercartao").css({display:"block"});');
			echo('$("#Errocartao").css({display:"none"});');} ?>
	};
	var sucesso = function(data){
		meio = data.TaxaMoIP;
$("#statusmoip").html("");
				$("#statusmoipadd").html("");
				$("#idmoip").html("");
				$("#moiperro").html("");
				$("#Errocartao").css({display:"block"});
				$("#statusmoip").css({display:"block"});
				$("#moiperro").css({display:"block"});
				$("#statusmoipadd").css({display:"block"});
				$("#idmoip").css({display:"block"});

		if (meio == undefined){
				$(".loader").css({display:"none"});
				$("#pgboletoedeb").removeAttr('disabled');
				<?php if($opcaopg['forma_pagamento'] == "BoletoBancario"){ ?>
				$(".modal-body").append("<iframe hspace=\"0\" vspace=\"0\" width=\"650\" height=\"900\"  scrolling= \"no\" frameBorder=\"0\" allowtransparency=\"true\" src=\"<?php echo $urldomoip ?>/Instrucao.do?token=<?php echo $url ?>\"> </iframe>");
				<?php } ?>
			}else{


				$("#pgcartao").removeAttr('disabled');
			if (data.Status == "Cancelado"){
				$("#refazercartao").css({display:"block"});
				$(".loader").css({display:"none"});
				$("#statusmoip").html("<h3>Pagamento Cancelado</h3>");
				$("#statusmoipadd").html("Transação não aprovada.");
				$("#idmoip").html("O número de sua transação no Moip é: ");
				$("#idmoip").html(data.CodigoMoIP);
				$("#moiperro").html("<h4>Motivo:</h4>");
				var motivo = JSON.stringify(data.Classificacao.Descricao);
				if(motivo == '"Desconhecido"'){
					$("#moiperro").html("Seus dados estão incorretos ou não podemos envia-los a operadora de crédito.");
				}
				if(motivo == '"Transação não processada"'){
					$("#moiperro").html("O pagamento não pode ser processado.</br>Por favor, tente novamente.</br>Caso o erro persista, entre em contato com o nosso atendimento.");
					
				}
				if(motivo == '"Política de segurança do Moip"'){
					$("#moiperro").html("Pagamento não autorizado.</br>Entre em contato com o seu banco antes de uma nova tentativa.");
				}
				if(motivo == '"Política de segurança do Banco Emissor"'){
					$("#moiperro").html("O pagamento não foi autorizado pelo Banco Emissor do seu Cartão.</br>Entre em contato com o Banco para entender o motivo e refazer o pagamento..");
				}
				if(motivo == '"Cartão vencido"'){
					$("#moiperro").html("A validade do seu Cartão expirou.</br>Escolha outra forma de pagamento para concluir o pagamento.");
				}
				if(motivo == '"Dados inválidos"'){
					$("#moiperro").html("Dados informados inválidos.</br>Você digitou algo errado durante o preenchimento dos dados do seu Cartão.</br>Certifique-se de que está usando o Cartão correto e faça uma nova tentativa.");
				}
			}
			if (data.Status == "EmAnalise"){
				$(".loader").css({display:"none"});
				$("#statusmoip").html("<h3>Pagamento Aguardando Aprovação</h3>");
				$("#statusmoipadd").html("Por favor, aguarde a em analise da transação. Assim que for alterado o status você será informado via e-mail.");
				$("#idmoip").html("O número de sua transação no Moip é: ");
				$("#idmoip").html(data.CodigoMoIP);
			}
			if (data.Status == "Autorizado"){
				$(".loader").css({display:"none"});
				$("#statusmoip").html("<h3>Pagamento Aprovado</h3>");
				$("#statusmoipadd").html("Por favor, aguarde o processo de envio.");
				$("#idmoip").html("O número de sua transação no Moip é: ");
				$("#idmoip").html(data.CodigoMoIP);
			}
		}
	};
	var erroValidacao = function(data) {
		for (i=0; i<data.length; i++) {
			Moip = data[i];
			infosMoip = 'Ops, parece que há algo errado:';
			for(j in Moip){
				atributo = j;
				if(atributo == "Mensagem"){
					valor = Moip[j];
					infosMoip += '<li class="erro" style="list-style: none;margin-left: 29px;font-weight: bold;">'+valor +'</li>';
					if(valor != "Informe o token da Instrução"){
					$("#refazercartao").css({display:"block"});
					}
				}
			}
			$("#Errocartao").html("");
			$("#Errocartao").css({display:"block"});
			$("#Errocartao").html(infosMoip);
			infosMoip = '';
			$(".loader").css({display:"none"});
<?php if($refazerpagamento == "1" && $opcaopg['forma_pagamento'] == "CartaoCredito" ) { echo('$("#Errocartao").html("");'); } ?>
<?php if($refazerpagamento == "1" && $opcaopg['forma_pagamento'] == "CartaoCredito" ) { echo('$("#Errocartao2").html(infosMoip);'); } ?>
<?php if($refazerpagamento == "1" && $opcaopg['forma_pagamento'] == "CartaoCredito" ) { echo('$(".loader").css({display:"none"});'); } ?>


		}

	};
	</script>
	<div id="Descr_pg">
	
	
	<?php if($opcaopg['forma_pagamento'] == "DebitoBancario"):?>
	<meta http-equiv='Refresh' content='5;URL=<?php echo $urldomoip ?>/Instrucao.do?token=<?php echo $url ?>'>
	<h3>Transferência Bancária</h3>
	Você será redirecionado para o seu banco em 5 segundos, caso isto não ocorra, por favor clique no link abaixo para ser redirecionado ao seu banco.</br></br>
	<button type="button" title="Finalizar compra" class="button btn-proceed-checkout btn-checkout" onclick="window.location='<?php echo $urldomoip ?>/Instrucao.do?token=<?php echo $url ?>'"><span><span>Ir ao Banco</span></span></button>
	<?php endif; ?>
	
	
	<?php if($opcaopg['forma_pagamento']== "BoletoBancario"):?>
	<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/bootstrap-modal.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/model.js'); ?>"></script>
	<h3>Boleto Bancário</h3>
	<h3>Clique no botão abaixo para imprimir o seu boleto.</h3>
	<button class="btn btn-primary btn buttonmoip" data-toggle="modal" href="#showXml" >Imprimir Boleto</button>
         </div>
          <div class="modal" id="showXml" >
            <div class="modal-header">
              <button class="close" data-dismiss="modal">x</button>
              <h3>Boleto Bancário - Bradesco</h3>
            </div>
          <div class="modal-body"></div>
	</div>
	<?php endif; ?>
	<?php 
	$tipoderecebimento = $standard->getConfigData('tipoderecebimento');
	if($tipoderecebimento == "0"):
		$tipoderecebimento = "Parcelado";
	else: 
		$tipoderecebimento = "AVista";	
	endif; 
	if($opcaopg['forma_pagamento'] == "CartaoCredito"): ?>
	<h3>Cartão de Crédito</h3>
	<div class="loader"/>Por favor, aguarde!</br><img src="<?php echo $this->getSkinUrl('o2ti_moip/imagem/ajax-loader.gif'); ?>" border="0"></div>
	<div id="Errocartao"></div>
	<script type="text/javascript">
	$(document).ready(function(){

	  $("#token").val($("#MoipWidget").attr("data-token"));

	  $("#sendToMoip2").click(function(){
	    sendToCreditCard2();
		$(".loader").css({display:"block"});
		$("#Errocartao").css({display:"none"});
		$("#statusmoip").css({display:"none"});
		$("#moiperro").css({display:"none"});
		$("#statusmoipadd").css({display:"none"});
		$("#idmoip").css({display:"none"});
			  });

	});
	sendToCreditCard2 = function() {
	    var settings = {
		"Forma": "CartaoCredito",
		"Instituicao": $("#instituicao").val(),
		"Parcelas": $("#Parcelas").val(),
		"Recebimento": "<?php echo $tipoderecebimento ?>",
		"CartaoCredito": {
		    "Numero": $("input[name=Numero]").val(),
		    "Expiracao": $("input[name=Expiracao]").val(),
		    "CodigoSeguranca": $("input[name=CodigoSeguranca]").val(),
		    "Portador": {
		        "Nome": $("input[name=Portador]").val(),
		        "DataNascimento": $("input[name=DataNascimento]").val(),
		        "Telefone": $("input[name=Telefone]").val(),
		        "Identidade": $("input[name=CPF]").val()
		    }
		}
	    }
	    MoipWidget(settings);
	 };
	</script>
	<div id="refazercartao" style="display:none;width:87%;">
<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/novamascara.js');?>"></script>
<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/validacao.js');?>"></script>
<script type="text/javascript" src="<?php echo $this->getSkinUrl('o2ti_moip/js/jquery.js'); ?>"></script>
		<div class="well">
			<h3>Não foi possível concluir o pagamento, por favor reveja os dados e tente novamente.</h3>
			<ul class="repay-form">
				<li>
					<h3>Dados do Cartão</h3>
				</li>
				<li>
					<label>Parcelas:</label>
					<select id="Parcelas" >
					<?php
						$valor = $totalparaparcelado;
						$parcelamento = $api->getParcelamento(number_format($valor, 2, ".", "."));
					?>
					<?php foreach ($parcelamento as $k => $v):?>
					<option value="<?php echo $k ?>"><?php echo $k . "x R$ "  . $v['valor'] . " | Total: R$ " .$v['total']."" ?></option>
					<?php endforeach; ?>				
					</select>
				</li>
				<li>
					<label>Institui&ccedil;&atilde;o:</label>
					<select id="instituicao">
					  <option value="Visa">Visa</option>
					  <option value="Mastercard">Mastercard</option>
					  <option value="AmericanExpress">AmericanExpress</option>
					  <option value="Hipercard">Hipercard</option>
					  <option value="Diners">Diners</option>	
					</select>				
				</li>
				<li>
					<div>
						<label>Numero do Cart&atilde;o:</label>
						<input type="text" id="Numero" name="Numero" value="">
					</div>
					<div>
						<label>CVV:</label>
						<input type="text" id="CodigoSeguranca" name="CodigoSeguranca" value="" size="4">
					</div>
				</li>
				<li>
					<label>Expira&ccedil;&atilde;o:</label>
					<input type="text" id="Expiracao" name="Expiracao" value=""  onkeypress="return txtBoxFormat(this, '99/99', event);" maxlength="5" placeholder="Ex. 10/15"  size="5">
				</li>
				<li>
					<h3>Dados do Títular do Cartão</h3>
				</li>
				<li>
					<label>Portador:</label>
					<input type="text" id="Portador" name="Portador" value="">
				</li>
				<li>
					<label>CPF:</label>
					<input type="text" id="CPF" name="CPF" value="">
				</li>
				<li>
					<label>Data Nascimento:</label>
					<input type="text" id="DataNascimento" name="DataNascimento" onkeypress="return txtBoxFormat(this, '99/99/9999', event);" maxlength="10" placeholder="Ex. 10/10/1985"  value=""><br>
				</li>
				<li>
					<label>Telefone:</label>
					<input type="text" id="Telefone" name="Telefone" value="" onkeypress="return txtBoxFormat(this, '(99)9999-9999', event);" placeholder="Ex. (11)9999-9999" maxlength="14" ><br>
				</li>
				<li>
					<button id="sendToMoip2" class="btn buttonmoip">Pagar Agora</button>
				</li>
			</ul>
		</div>
	</div>
	<div  id="pgcartao">
	<div id="statusmoip"></div>
	<div id="statusmoipadd"></div>
	<div id="moiperro"></div>
	<div id="idmoip"></div>
	</div>

	<?php endif; ?>
	
	</div>
	
	<div id="Descr_pedido" >
	<h3>Detalhes de seu pedido</h3>
	<div id="Descr_corpo">
	<h4>O Número do seu pedido é:</h4>
	#<?php echo $sale_id ?>
	<h4>Seu pedido será enviado por:</h4>
	<?php echo $order->getShippingDescription(); ?>
	</div>
	<div id="Descr_produto" >
	<h4>Descrição do seu pedido</h4>
	<div class="nome_prod_label">Nome</div>
	<div class="qty_prod_label">Qtd</div>
	<div class="price_prod_label">Preço</div>
	<?php $orderItems = $order -> getItemsCollection();
			foreach ($orderItems as $item) {
				$product_sku = $item -> sku;
				$product_name = $item -> getName();
				$_product = Mage::getModel('catalog/product') -> load($item->getProductId());
				$cats = $_product -> getCategoryIds();
				$category_name = Mage::getModel('catalog/category') -> load($cats[0]) -> getName();
				$price = $item->getPrice();
				$qty = $item -> getQtyOrdered();
				$product_image = $item -> getThumbnailUrl();
				echo '<div class="nome_prod_label">'.$product_name.'</div>';
				echo '<div class="qty_prod_label">'.number_format($qty, 0).'</div>';
				echo '<div class="price_prod_label">R$ '.number_format($price,2).'</div>';
			}
			echo '<div class="total_prod_label">Valor do pedido com eventuais descontos e frete de R$ '.number_format($order->getGrandTotal(),2).'</div>';
	?>
	</div>
</div>
   <?php

		return $html;
	}
}
