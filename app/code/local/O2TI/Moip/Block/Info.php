<?php

class O2TI_Moip_Block_Info extends Mage_Payment_Block_Info
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Moip/info.phtml');
    }
    
    protected function _beforeToHtml()
    {
        $this->_prepareInfo();
        return parent::_beforeToHtml();
    }

  
    public function getMoip()
    {
        return Mage::getSingleton('moip/standard');
    }

    protected function _toHtml()
    {
	$standard = Mage::getModel('moip/standard');
	$validopara_moip = $standard->getConfigData('validopara_moip');
        $html = '';
       	$order = Mage::getModel('sales/order');
        $titulo = $this->getMethod()->getTitle();
	$moip = $this->getMoip();
	$order = $this->getInfo()->getOrder()->getquote_id();
	$sale_id =  Mage::getSingleton('checkout/session')->getLastRealOrderId();

	Mage::log($order);
	
	
	
	
	
	if ($sale_id != ""){
	$connR = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "SELECT *
	          FROM moip
	          WHERE sale_id IN (".$sale_id.") AND status ='Sucesso'";
	$_venda = $connR->fetchAll($sql);
 	foreach($_venda as $venda)
	{
		$tokenpagamento = $venda['xml_return'];
		$bandeira = $venda['bandeira'];
		$Formadepagamento = $venda['formapg'];
		$orderid = $venda['sale_id'];
	}
	}
	else
	{
	$connR = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "SELECT *
	          FROM moip
	          WHERE realorder_id IN (".$order.") AND status ='Sucesso'";
	$_venda = $connR->fetchAll($sql);
 	foreach($_venda as $venda)
	{
		$tokenpagamento = $venda['xml_return'];
		$bandeira = $venda['bandeira'];
		$Formadepagamento = $venda['formapg'];
		$orderid = $venda['sale_id'];
	}
	}
	$formadepagamento = $this->getNomePagamento($Formadepagamento);
	$imagem = $this->getSkinUrl('o2ti_moip/imagem/'.$bandeira.'.png');
if($Formadepagamento == "BoletoBancario"){
	$html = "<b> Pago via </b>". $titulo .", por: </br><b>"  . $formadepagamento  . "</b></br><img src='" . $imagem . "'  style='width:80px;float:left;margin:5px;' border='0'>" . "<div id='repagar'><a href='https://www.moip.com.br/Instrucao.do?token=".$tokenpagamento."' style='background: none repeat scroll 0 0 #337BAA;margin:10px;border-radius:3px;padding:10px 5px;width:140px;float:left; text-align:center;color:#ffffff;font-weight:normal;font-size:13px;display:block;text-decoration:none;font-family:Arial,Helvetica,sans-serif'>Imprimir Boleto</a></div>";
}
if($Formadepagamento == "DebitoBancario"){
	$html = "<b> Pago via </b>". $titulo .", por: </br><b>"  . $formadepagamento  . "</b></br><img src='" . $imagem . "'  style='width:80px;float:left;margin:5px;' border='0'>" . "<div id='repagar'><a href='https://www.moip.com.br/Instrucao.do?token=".$tokenpagamento."' style='background: none repeat scroll 0 0 #337BAA;margin:10px;border-radius:3px;padding:10px 5px;width:140px;float:left; text-align:center;color:#ffffff;font-weight:normal;font-size:13px;display:block;text-decoration:none;font-family:Arial,Helvetica,sans-serif'>Realizar Transferência</a></div>";
}
if($Formadepagamento == "CartaoCredito"){


$params = array(
			'token'  => $venda['xml_return'],
			'orderId' =>  $venda['sale_id'],
			'realorder' =>  $venda['realorder_id'],
			);
$urldosite = Mage::getBaseUrl('web', true);			
$url = $urldosite."Moip/standard/redirect/";
	$html = "<b> Pago via </b>". $titulo .", por: </br><b>"  . $formadepagamento  . "</b></br><img src='" . $imagem . "'  style='width:80px;float:left;margin:5px;' border='0'>" . "<div id='repagar'><a href='".$url."token/".$tokenpagamento."/orderId/".$venda['realorder_id']."/realorder/".$venda['sale_id']."' style='background: none repeat scroll 0 0 #337BAA;margin:10px;border-radius:3px;padding:10px 5px;width:140px;float:left; text-align:center;color:#ffffff;font-weight:normal;font-size:13px;display:block;text-decoration:none;font-family:Arial,Helvetica,sans-serif'>Ja pagou? Pagar Agora</a></div>";
}
        

        return $html;
    }
    
	private function getNomePagamento($param) {
		$nome = "";
		switch ($param) {
		case "BoletoBancario":
		    $nome = "Boleto Bancário";
		    break;
		case "DebitoBancario":
		    $nome = "Debito Bancário";
		    break;
		case "CartaoCredito":
		    $nome = "Cartão de Crédito";
		    break;
		}
		return $nome;
	}
	

    protected function _prepareInfo()
    {
    
            $order = $this->getInfo()->getQuote();
       
        
       
      
	
       
        
    }
}
